package org.mdks.fcs_pack;

import org.ruboto.JRubyAdapter;
import org.ruboto.ScriptLoader;

public class MyReceiver extends org.ruboto.RubotoBroadcastReceiver {
    private boolean scriptLoaded = false;

    public MyReceiver() {
        super("my_receiver.rb");
        if (JRubyAdapter.isInitialized()) {
            scriptLoaded = true;
        }
    }

    public void onReceive(android.content.Context context, android.content.Intent intent) {
        if (!scriptLoaded) {
            if (JRubyAdapter.setUpJRuby(context)) {
                ScriptLoader.loadScript(this);
                scriptLoaded = true;
            } else {
                // FIXME(uwe): What to do if the Ruboto Core platform is missing?
            }
        }
        super.onReceive(context, intent);
    }

}
